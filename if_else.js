// Bài 1: Cho người dùng nhập vào 3 số nguyên. Viết chương trình xuất 3 số theo thứ tự tăng dần
/*
input: 3 số nguyên
action:
+ lấy giá trị từ người dùng nhập vào
+ kiểm tra giá trị đó là số nguyên hay không. Nếu không phải thì xuất ra màn hình "không phải là số nguyên"
nếu phải thì thực hiện phép so sánh ở 6 trường hợp:
TH1: a>b>c thì xuất ra màn hình c,b,a
TH2: a>c>b thì xuất ra màn hình b,c,a
TH3: b>c>a thì xuất ra màn hình a,c,b
TH4: b>a>c thì xuất ra màn hình c,a,b
TH5: c>a>b thì xuất ra màn hình b,a,c
TH6: c>b>a thì xuất ra màn hình a,b,c
output: xuất 3 số theo thứ tự tăng dần
*/

function Sosanh(){
    const a=document.getElementById("number1").value*1
    const b=document.getElementById("number2").value*1
    const c=document.getElementById("number3").value*1
    const result=document.getElementById("result")
    if(Number.isInteger(a)&&Number.isInteger(b)&&Number.isInteger(c)){
        if(a>b&&b>c){result.innerHTML=`${c}, ${b}, ${a}`}
        else if(a>c&&c>b){result.innerHTML=`${b}, ${c}, ${a}`}
        else if(b>a&&a>c){result.innerHTML=`${c}, ${a}, ${b}`}
        else if(b>c&&c>a){result.innerHTML=`${a}, ${c}, ${b}`}
        else if(c>a&&a>b){result.innerHTML=`${b}, ${a}, ${c}`}
        else if(c>b&&c>a){result.innerHTML=`${a}, ${b}, ${c}`}
    }else{
        result.innerHTML="Đây không phải là số nguyên, xin vui lòng nhập lại"
    }
}

// Bài 2: Viết chương trình “Chào hỏi” các thành viên trong gia đình với các đặc điểm. Đầu tiên máy sẽ
// hỏi ai sử dụng máy. Sau đó dựa vào câu trả lời và đưa ra lời chào phù hợp. Giả sử trong gia
// đình có 4 thành viên: Bố (B), Mẹ (M), anh Trai (A) và Em gái (E)
/*
input: câu hỏi của máy, câu trả lời của 4 thành viên: Bố (B), Mẹ (M), anh Trai (A) và Em gái (E)
action:
+ Đưa ra 4 option nếu option rồi lấy value. (nếu là Bố thì xuất ra câu trả lời chào bố.....)
output: đưa ra lời chào phù hợp
*/
function hello(){
    const  a=document.querySelector('input[name="selector"]:checked').value
    const result1=document.getElementById("result1")
    if(a==="bo"){
        result1.innerHTML="Hello Bố"
    }else if(a==="me"){
        result1.innerHTML="Hello Mẹ"
    }else if(a==="anh"){
        result1.innerHTML="Hello Anh"
    }else if(a==="em"){
        result1.innerHTML="Hello Em"
    }
}
// Bài 3: Cho 3 số nguyên. Viết chương trình xuất ra có bao nhiêu số lẻ và bao nhiêu số chẵn.
/*
input: 3 số nguyên
action:
+ cho i=0
+ áp dụng a % 2 == 0 để xác định số chẵn, =>i++;
+ số lượng số lẻ =>3-i
output: bao nhiêu số lẻ và bao nhiêu số chẵn
*/

function chanle(){
    const d=document.getElementById("number4").value*1
    const e=document.getElementById("number5").value*1
    const f=document.getElementById("number6").value*1
    const result2=document.getElementById("result2")
    if(Number.isInteger(d)&&Number.isInteger(e)&&Number.isInteger(f)){
        let i=0
        if(d%2==0){i++}
        if(e%2==0){i++}
        if(f%2==0){i++}
        result2.innerHTML=`Số lượng số chẵn: ${i}, số lượng số lẻ: ${3-i}`
    }else{
        result2.innerHTML="Đây không phải là số nguyên, xin vui lòng nhập lại"
    }
}
// Bài 4: Viết chương trình cho nhập 3 cạnh của tam giác. Hãy cho biết đó là tam giác gì?
/*
input: 3 cạnh tam giác
action:
+ so sánh 3 cạnh tam giác
+ nếu a=b=c thì tam giác đều
+ ....
+ số lượng số lẻ =>3-i
output: xác định được đây là tam giác gì
*/

function tamgiac(){
    const c1=document.getElementById("c1").value*1
    const c2=document.getElementById("c2").value*1
    const c3=document.getElementById("c3").value*1
    const result3=document.getElementById("result3")
        if(c1==c2&c2==c3&c1==c3){
            result3.innerHTML="Đây là tam giác đều"
        }else if(c1==c2||c1==c3||c2==c3){
            result3.innerHTML="Đây là tam giác cân"
        }else if(c1==Math.sqrt(c2*c2+c3*c3)||c2==Math.sqrt(c1*c1+c3*c3)||c3==Math.sqrt(c2*c2+c1*c1)){
            result3.innerHTML="Đây là tam giác vuông"
        }else{
            result3.innerHTML="Đây là tam giác thường"
        }
}